rem add the path to THIS script (the one you're reading) to PATH variable
rem add the path to phpstorm64.exe to PATH variable
rem add the path to mysql.exe to PATH variable
rem open command prompt in source code directory (to run artisan commands and git commands)
rem open command prompt in notes directory (to create this word document and run git commands)
rem open mysql command prompt window in mysql directory (to work with the database)
rem open phpstorm IDE program (to edit the project)
rem open google chrome browser, with the URL of my project (to test the project)
rem open windows explorer window in notes directory (to create this word document)
echo off
cls
cd /d "C:\Apache24\htdocs\swiperight"
start /d "C:\Apache24\htdocs\swiperight"
@echo on
rem REMINDER: git push -u origin master
@echo off
cd "C:\Program Files\MySQL\MySQL Server 5.7\bin"
start "C:\Program Files\MySQL\MySQL Server 5.7\bin\mysql.exe"
start chrome http://swiperighttoapply.dev
start explorer.exe "C:\Apache24\htdocs\swiperight"