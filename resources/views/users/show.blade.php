@extends('pages.master')

{{--{!! Form::open([]) !!}--}}

{{--{!! Form::text('name', @$user->firstname) !!}--}}

{{--{!! Form::password('password') !!}--}}

{{--{!! Form::submit('Send') !!}--}}

{{--{!! Form::close() !!}--}}


@section('title')
    {{$user->firstname}} {{$user->lastname}}
@stop

@section('header')
    SwipeRightToApply: {{$user->firstname}} {{$user->lastname}}
@stop

@section('data')
    <div>

        {{--{{print_r($user->getAttributes())}}--}}
        {{--{{print_r($user->toArray())}}--}}

        {{--{!! Form::open([]) !!}--}}
            {{--{!! Form::text('name', @$user->firstname) !!}--}}
            {{--{!! Form::password('password') !!}--}}
            {{--{!! Form::submit('Send') !!}--}}
        {{--{!! Form::close() !!}--}}

        <table name="usertable">
            @foreach($user->toArray() as $key=>$value)
                <tr>
                    <td>
                        {{$key}}
                    </td>
                    <td>
                        {{$value}}
                    </td>
                </tr>
            @endforeach
        </table>

    </div>

    <div class="links">
        <a href="mailto:admin@swiperighttoapply.com?subject=swipe right to apply, help">Email</a>
        <a href="https://twitter.com/RightToApply">Twitter</a>
        <a href="https://www.facebook.com/swiperighttoapply">Facebook</a>
        <a href="https://developer.apple.com/app-store/">iOS app</a>
    </div>
@stop

@section('footer')
    <hr>
    Thank you for using SwipeRightToApply
@stop