<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'email' => $faker->unique()->email,
        'linkedinurl' => $faker->url,
        'streetaddress' => $faker->streetAddress,
        'city' => $faker->city,
        'state' => $faker->state,
        'countryid' => $faker->numberBetween(1, 249),
        'postalzipcode' => $faker->postcode,
        'workphone' => $faker->phoneNumber,
        'workphoneextension' => $faker->numberBetween(0, 1000),
        'mobilephone' => $faker->unique()->phoneNumber,
        'homephone' => $faker->phoneNumber,
        //'password' => $faker->unique()->email,
        'middlename' => $faker->firstName,

        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});