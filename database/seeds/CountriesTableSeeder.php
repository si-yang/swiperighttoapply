<?php

namespace database\seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountriesTableSeeder extends \DatabaseSeeder {

    // create Countries table records
    // that WILL be created in the database

    public function run() {
        DB::table('countries')->delete();

        $countries = array(
            array(
                'id' => 1,
                'code' => 'US',
                'name' => 'United States'
            ),
            array(
                'id' => 2,
                'code' => 'CA',
                'name' => 'Canada'
            ),
            array(
                'id' => 3,
                'code' => 'AF',
                'name' => 'Afghanistan'
            ),
            array(
                'id' => 4,
                'code' => 'AL',
                'name' => 'Albania'
            ),
            array(
                'id' => 5,
                'code' => 'YE',
                'name' => 'Yemen'
            ),
            array(
                'id' => 6,
                'code' => 'YU',
                'name' => 'Yugoslavia'
            ),
            array(
                'id' => 7,
                'code' => 'ZR',
                'name' => 'Zaire'
            ),
            array(
                'id' => 8,
                'code' => 'ZM',
                'name' => 'Zambia'
            ),
            array(
                'id' => 9,
                'code' => 'ZW',
                'name' => 'Zimbabwe'
            ),
            array(
                'id' => 10,
                'code' => 'VG',
                'name' => 'Virgin Islands (British)'
            ),
        );

        DB::table('countries')->insert($countries);
    }
}